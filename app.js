const inputType = document.getElementById("input-type");
const inputAmount = document.getElementById("input-amount");
const btnSave = document.getElementById("btn-save");
const spendingList = document.getElementById("spending-list");
const totalOutput = document.getElementById("output");

let total = 0;

function clear() {
  inputType.value = "";
  inputAmount.value = "";
}
btnSave.addEventListener("click", () => {
  const tipo = inputType.value;
  const amount = inputAmount.value;
  if (tipo.trim().length > 0 && amount.trim().length > 0 && amount > 0) {
    //We add the item to the list element
    const newItem = document.createElement("ion-item");
    newItem.textContent = tipo + ": $" + amount;
    spendingList.appendChild(newItem);

    //Update total
    total += +amount;
    totalOutput.textContent = total;

    //clear inputs
    clear();
  } else {
    alertController
      .create({
        message: "Please specify a type and an amount",
        header: "Invalid values",
        buttons: ["Ok"],
      })
      .then((alertElement) => {
        alertElement.present();
      });
  }
});
